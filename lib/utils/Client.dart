

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';



import 'package:anonymousmessenger/model/Message.dart';


class Client {

  static Client _client;
  static String endSeq = "###END";

  static Client getInst() {
    if (_client == null) {
      _client = new Client();
    }
    return _client;
  }


  String host = "hexagone.ddns.net";
  int port = 25565;
  Socket out;
  String _message = "";
  int _readedCount = 0;
  List<Completer> _msgs = List();
  StreamSubscription<Uint8List> subscription;

  Future<void> startListen() async {
    _msgs.clear();
    _msgs.add(Completer());

    out.listen(
            (event) {

          _message += String.fromCharCodes(Uint8List
              .fromList(event)
              .buffer
              .asUint8List());
          if (_message.endsWith(endSeq)) {
            var list = _message.split(endSeq);
            list.removeLast();
            list.forEach((element) {
              var map = json.decode(element);
              _msgs.elementAt(_msgs.length - 1).complete(Message.fromJson(map));
              _msgs.add(Completer());
            });
            _message = "";
          }
        },
        onDone: () {print("Done");},
        onError: (e) {print("Error" + e);},
        cancelOnError: false);
    await Future.delayed(Duration(minutes: 60000));
  }

  Future<void> connect() async {
    out = await Socket.connect(host, port);

  }

  void sendMsg(Message msg) {
    var json = msg.toJson().toString();
    print(json);
    out.write(json + endSeq);
  }

  Future<Message> getLastMsg() async {
    _readedCount++;
    int lastId = _msgs.length;
    return await _msgs.elementAt(lastId - 1).future;
  }

  List<Completer> get() => _msgs;
}


