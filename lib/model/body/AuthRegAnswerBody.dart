


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class AuthRegAnswerBody extends MsgBody {
  bool Success;
  String Desc;

  AuthRegAnswerBody({this.Success, this.Desc});

  factory AuthRegAnswerBody.fromJson(Map<String, dynamic> json) {
    return AuthRegAnswerBody(
      Success: json['Success'] as bool,
      Desc: json['Desc'] as String,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Success\"': "\""+this.Success.toString()+"\"",
    '\"Desc\"': "\""+this.Desc+"\"",
  };

}