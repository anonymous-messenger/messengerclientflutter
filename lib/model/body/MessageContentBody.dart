import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class MessageContentBody extends MsgBody {

  String Time;
  String From;
  String Text;
  int ChannelId;
  int PrivateChannelId;

  MessageContentBody({this.From, this.Text, this.ChannelId, this.PrivateChannelId, this.Time});

  factory MessageContentBody.fromJson(Map<String, dynamic> json) {
    return MessageContentBody(
      Time: json['Time'] as String,
      From: json['From'] as String,
      Text: json['Text'] as String,
      ChannelId: json['ChannelId'] as int,
      PrivateChannelId: json['PrivateChannelId'] as int,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"From\"': "\"" + this.From + "\"",
    '\"Time\"': "\"" + this.Time + "\"",
    '\"Text\"': "\""+this.Text+"\"",
    '\"ChannelId\"': "\""+this.ChannelId.toString()+"\"",
    '\"PrivateChannelId\"': "\""+this.PrivateChannelId.toString()+"\"",
  };

}