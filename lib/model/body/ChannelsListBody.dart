

import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';
import 'package:json_annotation/json_annotation.dart';

class Channel {
  int Id;
  String Name;

  Channel({this.Id, this.Name});

  factory Channel.fromJson(Map<String, dynamic> json)  {
    return Channel(
      Id: json['Id'] as int,
      Name: json['Name'] as String,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Id\"': this.Id.toString(),
    '\"Name\"': "\"" + this.Name + "\"",
  };

}


@JsonSerializable()
class ChannelsListBody extends MsgBody {
  List<Channel> Channels;

  ChannelsListBody({this.Channels});

  String getJsonChannels() {
    String json = "[";
    for(int i = 0; i < Channels.length; ++i) {

      json += Channels[i].toJson().toString();
      if (i != Channels.length - 1) {
        json += ",";
      }
    }
    return json + "]";
  }

  factory ChannelsListBody.fromJson(Map<String, dynamic> json) {
    return ChannelsListBody(
      Channels: (json['Channels'] as List)
          ?.map((e) =>
      e == null ? null : Channel.fromJson(e as Map<String, dynamic>))
          ?.toList(),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Channels\"': this.getJsonChannels(),
  };
}