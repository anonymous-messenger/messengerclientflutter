


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class SetMarkRequestBody extends MsgBody {

  String UserTo;
  int PChannelId;
  int Count;

  SetMarkRequestBody({this.UserTo, this.PChannelId, this.Count});

  factory SetMarkRequestBody.fromJson(Map<String, dynamic> json) {
    return SetMarkRequestBody(
      UserTo: json['UserTo'] as String,
      PChannelId: json['PChannelId'] as int,
      Count: json['Count'] as int,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"UserTo\"': "\"" + this.UserTo + "\"",
    '\"PChannelId\"': "\"" + this.PChannelId.toString() + "\"",
    '\"Count\"': "\"" + this.Count.toString() + "\"",
  };

}