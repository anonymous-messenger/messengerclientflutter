

import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class AuthRegBody extends MsgBody {

  String UserName;
  String Gender;
  int Age;
  String Pass;

  AuthRegBody({this.UserName, this.Gender, this.Age, this.Pass});

  factory AuthRegBody.fromJson(Map<String, dynamic> json)  {
    return AuthRegBody(
      UserName: json['UserName'] as String,
      Gender: json['Gender'] as String,
      Age: json['Age'] as int,
      Pass: json['Pass'] as String,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"UserName\"': "\""+this.UserName+"\"",
    '\"Gender\"': "\""+this.Gender+"\"",
    '\"Age\"': "\""+this.Age.toString()+"\"",
    '\"Pass\"': "\""+this.Pass+"\"",
  };
}