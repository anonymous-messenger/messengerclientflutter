

import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';
import 'package:json_annotation/json_annotation.dart';

class RandChatRequestBody extends MsgBody {
  String Gender;
  int MaxAge;
  int MinAge;
  int PChannelId;
  bool Abort;

  RandChatRequestBody({this.Gender, this.MaxAge, this.MinAge, this.Abort, this.PChannelId});

  factory RandChatRequestBody.fromJson(Map<String, dynamic> json){
    return RandChatRequestBody(
      Gender: json['Gender'] as String,
      MaxAge: json['MaxAge'] as int,
      MinAge: json['MinAge'] as int,
      Abort: json['Abort'] as bool,
      PChannelId: json['PChannelId'] as int,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Gender\"': "\"" + this.Gender + "\"",
    '\"MaxAge\"': "\"" + this.MaxAge.toString() + "\"",
    '\"MinAge\"': "\"" + this.MinAge.toString() + "\"",
    '\"PChannelId\"': "\"" + this.PChannelId.toString() + "\"",
    '\"Abort\"': "\"" + this.Abort.toString() + "\"",
  };

}