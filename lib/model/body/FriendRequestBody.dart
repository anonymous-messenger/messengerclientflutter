


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';



class FriendRequestBody extends MsgBody {

  bool Allow;
  String UserFrom;
  String UserTo;
  int FPChannelId;

  FriendRequestBody({this.UserFrom, this.UserTo, this.FPChannelId, this.Allow});

  factory FriendRequestBody.fromJson(Map<String, dynamic> json) {
    return FriendRequestBody(
      UserFrom: json['UserFrom'] as String,
      Allow: json['Allow'] as bool,
      UserTo: json['UserTo'] as String,
      FPChannelId: json['FPChannelId'] as int,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Allow\"': "\"" + this.Allow.toString() + "\"",
    '\"UserFrom\"': "\"" + this.UserFrom + "\"",
    '\"UserTo\"': "\"" + this.UserTo + "\"",
    '\"FPChannelId\"': "\"" + this.FPChannelId.toString() + "\"",
  };

}