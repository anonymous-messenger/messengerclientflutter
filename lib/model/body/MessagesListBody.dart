


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/MessageContentBody.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class MessagesListBody extends MsgBody {
  List<MessageContentBody> Messages;

  MessagesListBody({this.Messages});

  String getJsonMessages() {
    String json = "[";
    for(int i = 0; i < Messages.length; ++i) {

      json += Messages[i].toJson().toString();
      if (i != Messages.length - 1) {
        json += ",";
      }
    }
    return json + "]";
  }

  factory MessagesListBody.fromJson(Map<String, dynamic> json) {
    return MessagesListBody(
      Messages: (json['Messages'] as List)
          ?.map((e) =>
      e == null ? null : MessageContentBody.fromJson(e as Map<String, dynamic>))
          ?.toList(),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Messages\"': this.getJsonMessages(),
  };

}