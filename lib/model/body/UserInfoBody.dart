


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';

class UserInfoBody extends MsgBody {

  String Name;
  String Gender;
  int Age;
  int Score;

  UserInfoBody({this.Name, this.Age, this.Gender, this.Score});

  factory UserInfoBody.fromJson(Map<String, dynamic> json) {
    return UserInfoBody(
      Name: json['Name'] as String,
      Age: json['Age'] as int,
      Gender: json['Gender'] as String,
      Score: json['Score'] as int,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Name\"': "\""+this.Name+"\"",
    '\"Gender\"': "\""+this.Gender+"\"",
    '\"Age\"': "\""+this.Age.toString()+"\"",
    '\"Score\"': "\""+this.Score.toString()+"\"",
  };
}