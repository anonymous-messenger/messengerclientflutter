

class Command {
  static String NO_MSG = "NO_MSG";
  static String REGISTER = "REGISTER";
  static String AUTH = "AUTH";
  static String AUTH_ANSWER = "AUTH_ANSWER";
  static String DISCONNECT = "DISCONNECT";


  static String SEND_ANONYMOUS_MSG = "ANONYMOUS_MSG";
  static String SEND_PERSONAL_MSG = "PERSONAL_MSG";
  static String SEND_GROUP_MSG = "GROUP_MSG";

  static String RAND_CHAT_REQUEST = "RAND_CHAT_REQUEST";

  static String REPLY_TO_ANONYMOUS_MSG = "REPLY_ANONYMOUS_MSG";
  static String REPLY_TO_PERSONAL_MSG = "REPLY_PERSONAL_MSG";
  static String REPLY_TO_GROUP_MSG = "REPLY_GROUP_MSG";


  static String REQUEST_FOR_FRIENDSHIP = "REQUEST_FOR_FRIENDSHIP";
  static String ALLLOW_FRIENDSHIP = "ALLOW_FRIENDSHIP";
  static String DENY_FRIENDSHIP = "DENY_FRIENDSHIP";

  static String SET_USER_MARK = "SET_USER_MARK";
  static String LEAVE_CHAT = "LEAVE_CHAT";


  static String GET_CATEGORIES = "GET_CATEGORIES";
  static String GET_MESSAGES = "GET_MESSAGES";
  static String GET_CHANNELS = "GET_CHANNELS";
  static String GET_USER_INFO = "GET_USER_INFO";


//public static readonly string
}
