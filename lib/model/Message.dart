

import 'package:anonymousmessenger/model/body/FriendRequestBody.dart';
import 'package:anonymousmessenger/model/body/MessagesListBody.dart';
import 'package:anonymousmessenger/model/body/RandChatRequestBody.dart';
import 'package:anonymousmessenger/model/body/SetMarkRequestBody.dart';


import 'body/AuthRegAnswerBody.dart';
import 'body/AuthRegBody.dart';
import 'body/UserInfoBody.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/ChannelsListBody.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/MessageContentBody.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';



class Message {
  String Command;
  MsgBody Body;

  Message({this.Command, this.Body});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      Command: json['Command'] as String,
      Body: json['Body'] == null
          ? null
          : getBody(json['Body'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '\"Command\"': "\"" + this.Command + "\"",
    '\"Body\"': this.Body?.toJson(),
  };



}

MsgBody getBody(Map<String, dynamic> json) {
  if (json['Success'] != null) {
    return AuthRegAnswerBody.fromJson(json);
  } else if (json['Pass'] != null) {
    return AuthRegBody.fromJson(json);
  } else if (json['Text'] != null) {
    return MessageContentBody.fromJson(json);
  } else if (json['Score'] != null) {
    return UserInfoBody.fromJson(json);
  } else if (json['Channels'] != null) {
    return ChannelsListBody.fromJson(json);
  } else if (json['Messages'] != null) {
    return MessagesListBody.fromJson(json);
  } else if (json['Abort'] != null) {
    return RandChatRequestBody.fromJson(json);
  } else if (json['FPChannelId'] != null) {
    return FriendRequestBody.fromJson(json);
  } else if (json['Count'] != null) {
    return SetMarkRequestBody.fromJson(json);
  }
}