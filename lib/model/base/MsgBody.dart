

import 'package:json_annotation/json_annotation.dart';


part 'MsgBody.g.dart';


@JsonSerializable()
class MsgBody {
  MsgBody();

  factory MsgBody.fromJson(Map<String, dynamic> json) => _$MsgBodyFromJson(json);

  Map<String, dynamic> toJson() => _$MsgBodyToJson(this);

}