


import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/model/body/UserInfoBody.dart';
import 'package:anonymousmessenger/utils/Client.dart';

class UserInfo {
  static UserInfo _inst;

  String name = "";
  String gender = "";
  int age = 0;
  int score = 0;

  static UserInfo getInfo() {
    if (_inst == null) {
      _inst = UserInfo();
    }
    return _inst;
  }

  Future<void> update() async {
    Client.getInst().sendMsg(
        Message(
          Body: null,
          Command: Command.GET_USER_INFO,
        )
    );

    Message m = await Client.getInst().getLastMsg();

    if (m.Command == Command.GET_USER_INFO) {
      UserInfoBody b = m.Body as UserInfoBody;
      UserInfo.getInfo().name = b.Name;
      UserInfo.getInfo().age = b.Age;
      UserInfo.getInfo().score = b.Score;
      UserInfo.getInfo().gender = b.Gender;
    }


  }
}