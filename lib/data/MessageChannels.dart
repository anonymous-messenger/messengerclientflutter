


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/ChannelsListBody.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/utils/Client.dart';



class MessageChannels {
  static MessageChannels _inst;

  ChannelsListBody body;

  static MessageChannels getInfo() {
    if (_inst == null) {
      _inst = MessageChannels();
    }
    return _inst;
  }

  Future<void> update() async {
    Client.getInst().sendMsg(
        Message(
          Body: null,
          Command: Command.GET_CHANNELS,
        )
    );

    Message m = await Client.getInst().getLastMsg();

    if (m.Command == Command.GET_CHANNELS) {
      body = m.Body as ChannelsListBody;
    }

  }
}