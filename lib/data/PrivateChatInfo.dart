


import 'dart:async';

import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/MessageContentBody.dart';

class PrivateChatInfo {
  static PrivateChatInfo _privateChatInfo;
  static PrivateChatInfo getInfo() {
    if (_privateChatInfo == null) {
      _privateChatInfo = PrivateChatInfo();
    }
    return _privateChatInfo;
  }

  bool friendshipRequestSended = false;
  bool likeSetted = false;
  bool dislikeSetted = false;
  bool awaitConnection = false;
  bool connected = false;
  List<MessageContentBody> msgs = List<MessageContentBody>();
  int channelId;
}