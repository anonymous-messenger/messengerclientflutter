

import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'SimpleText.dart';

enum Action{
  Yes,
  No,
  Ok,
  Cancel,
}


VoidCallback procActionCallback({
  BuildContext context,
  String text,
  bool showYes = false,
  bool showNo = false,
  bool showOk = true,
  bool showCancel = false,
  Function onYes,
  Function onNo,
  Function onCancel,
  Function onOk})
{


  return () async {
    double fontSize = MediaQuery.of(context).size.width * 0.06;
    switch(await showDialog<Action>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: CColors.Main1C,
            title: Center(
              child: SimpleText(
                text: text,
                color: CColors.LightA,
                size: fontSize,
              ),
            ),
            children: <Widget>[
              Row(
                children: <Widget>[
                  if (showYes)
                    Spacer(flex: 1,),
                  if (showYes)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Yes);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionYes,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showNo)
                    Spacer(flex: 1,),
                  if (showNo)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.No);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionNo,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showCancel)
                    Spacer(flex: 1,),
                  if (showCancel)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Cancel);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionCancel,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showOk)
                    Spacer(flex: 1,),
                  if (showOk)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Ok);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionOk,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),
                  Spacer(flex: 1,),
                ],
              ),

            ],
          );
        })) {
      case Action.Yes:
        {
          onYes();
        }
        break;
      case Action.No:
        {
          onNo();
        }
        break;
      case Action.Ok:
        {
          onOk();

        }
        break;
      case Action.Cancel:
        {
          onCancel();

        }
        break;
    }
  };
}


void procAction({
  BuildContext context,
  String text,
  bool showYes = false,
  bool showNo = false,
  bool showOk = true,
  bool showCancel = false,
  Function onYes,
  Function onNo,
  Function onCancel,
  Function onOk})
async {



    double fontSize = MediaQuery.of(context).size.width * 0.06;
    switch(await showDialog<Action>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: CColors.Main1C,
            title: Center(
              child: SimpleText(
                text: text,
                color: CColors.LightA,
                size: fontSize,
              ),
            ),
            children: <Widget>[
              Row(
                children: <Widget>[
                  if (showYes)
                    Spacer(flex: 1,),
                  if (showYes)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Yes);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionYes,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showNo)
                    Spacer(flex: 1,),
                  if (showNo)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.No);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionNo,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showCancel)
                    Spacer(flex: 1,),
                  if (showCancel)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Cancel);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionCancel,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),

                  if (showOk)
                    Spacer(flex: 1,),
                  if (showOk)
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Action.Ok);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).dialogActionOk,
                        color: CColors.LightA,
                        size: fontSize,
                      ),
                    ),
                  Spacer(flex: 1,),
                ],
              ),

            ],
          );
        })) {
      case Action.Yes:
        {
          onYes();
        }
        break;
      case Action.No:
        {
          onNo();
        }
        break;
      case Action.Ok:
        {
          onOk();

        }
        break;
      case Action.Cancel:
        {
          onCancel();

        }
        break;
    }
}