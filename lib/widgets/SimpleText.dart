import 'package:flutter/cupertino.dart';

class SimpleText extends StatelessWidget{
  final String text;
  final double size;
  final Color color;
  const SimpleText({@required this.text, @required this.size, @required this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: size),
    );
  }
}