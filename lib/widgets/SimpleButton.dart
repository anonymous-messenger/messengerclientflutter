import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleButton extends StatelessWidget{
  final Widget child;
  final double minWidth;
  final double height;
  const SimpleButton({@required this.child, @required this.minWidth, @required this.height});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(

      buttonColor: CColors.Main1C,
      focusColor: CColors.Main1C,
      splashColor: CColors.OrangeA,
      hoverColor: CColors.Main1C,
      disabledColor: CColors.Main1C,
      highlightColor: CColors.Main1C,
      child: child,
      minWidth: minWidth,
      height: height,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
    );
  }
}