import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';

class GLocalizations {
  GLocalizations(this.locale);

  final Locale locale;

  static GLocalizations of(BuildContext context) {
    return Localizations.of<GLocalizations>(context, GLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'startScreenTextHeadLogin' : 'Login',
      'startScreenTextUserName' : 'User name',
      'startScreenTextPassword' : 'Password',


      'errorFillName' : 'Fill User name',
      'errorFillPass' : 'Fill Password',
      'errorSelectGender' : 'Select Gender',
      '' : '',
      '' : '',

      'dialogActionOk' : 'Ok',
      'dialogActionCancel' : 'Cancel',
      'dialogActionYes' : 'Yes',
      'dialogActionNo' : 'No',

      'registerScreenHead' : 'Register',
      'registerScreenAge' : 'Age',
      'registerScreenUserName' : 'User name',
      'registerScreenGender' : 'Gender',
      'registerScreenPassword' : 'Password',


      'dialogExitAppText' : 'Exit?',


      'userInfoHead' : 'Profile',
      'userInfoName' : 'Name',
      'userInfoGender' : 'Gender',
      'userInfoAge' : 'Age',
      'userInfoScore' : 'Score',


      'channelsTextHead' : 'Messages',

      'randChatHead' : 'Random Chat',
      'randChatGender' : 'Gender',
      'randChatAgeMin' : 'Min age',
      'randChatAgeMax' : 'Max age',
      'randChatClose' : 'Leave chat?',
      'randChatAchiveLike' : 'You got a like',
      'randChatAchiveDislike' : 'You got a dislike',
      'randCharFriendRequest' : 'The user wants to make friends',
      'randChatAllowFiendship' : 'Friendship allowed',
      'randChatDenyFriendship' : 'Friendship rejected',
      'randChatLeave' : 'User leave from chat',
      '' : '',
      '' : '',
    },
    'ru': {
      'startScreenTextHeadLogin' : 'Авторизация',
      'startScreenTextUserName' : 'Имя пользователя',
      'startScreenTextPassword' : 'Пароль',


      'errorFillName' : 'Введите имя',
      'errorFillPass' : 'Введите пароль',
      'errorSelectGender' : 'Выберите пол',
      '' : '',
      '' : '',


      'dialogActionOk' : 'Ок',
      'dialogActionCancel' : 'Отмена',
      'dialogActionYes' : 'Да',
      'dialogActionNo' : 'Нет',

      'dialogExitAppText' : 'Выйти?',

      'registerScreenHead' : 'Регистрация',
      'registerScreenAge' : 'Возраст',
      'registerScreenUserName' : 'Имя пользователя',
      'registerScreenGender' : 'Пол',
      'registerScreenPassword' : 'Пароль',


      'userInfoHead' : 'Профиль',
      'userInfoName' : 'Имя',
      'userInfoGender' : 'Пол',
      'userInfoAge' : 'Возраст',
      'userInfoScore' : 'Очки',

      'channelsTextHead' : 'Сообщения',

      'randChatHead' : 'Случайный чат',
      'randChatGender' : 'Пол собеседника',
      'randChatAgeMin' : 'Минимальный возраст',
      'randChatAgeMax' : 'Максимальный возраст',
      'randChatClose' : 'Покинуть чат?',
      'randChatAchiveLike' : 'Вы получили лайк',
      'randChatAchiveDislike' : 'Вы получили дизлайк',
      'randCharFriendRequest' : 'Пользователь хочет подружиться',
      'randChatAllowFiendship' : 'Разрешить дружбу',
      'randChatDenyFriendship' : 'Отклонить дружбу',
      'randChatLeave' : 'Пользователь покинул чат',
      '' : '',

    },
  };


  String get startScreenTextHeadLogin => _localizedValues[locale.languageCode]['startScreenTextHeadLogin'];
  String get startScreenTextUserName => _localizedValues[locale.languageCode]['startScreenTextUserName'];
  String get startScreenTextPassword => _localizedValues[locale.languageCode]['startScreenTextPassword'];
  String get dialogActionOk => _localizedValues[locale.languageCode]['dialogActionOk'];
  String get dialogActionCancel => _localizedValues[locale.languageCode]['dialogActionCancel'];
  String get dialogActionYes => _localizedValues[locale.languageCode]['dialogActionYes'];
  String get dialogActionNo => _localizedValues[locale.languageCode]['dialogActionNo'];
  String get registerScreenHead => _localizedValues[locale.languageCode]['registerScreenHead'];
  String get registerScreenAge => _localizedValues[locale.languageCode]['registerScreenAge'];
  String get registerScreenUserName => _localizedValues[locale.languageCode]['registerScreenUserName'];
  String get registerScreenGender => _localizedValues[locale.languageCode]['registerScreenGender'];
  String get registerScreenPassword => _localizedValues[locale.languageCode]['registerScreenPassword'];
  String get errorFillName => _localizedValues[locale.languageCode]['errorFillName'];
  String get errorFillPass => _localizedValues[locale.languageCode]['errorFillPass'];
  String get errorSelectGender => _localizedValues[locale.languageCode]['errorSelectGender'];
  String get dialogExitAppText => _localizedValues[locale.languageCode]['dialogExitAppText'];
  String get userInfoHead => _localizedValues[locale.languageCode]['userInfoHead'];
  String get userInfoName => _localizedValues[locale.languageCode]['userInfoName'];
  String get userInfoGender => _localizedValues[locale.languageCode]['userInfoGender'];
  String get userInfoAge => _localizedValues[locale.languageCode]['userInfoAge'];
  String get userInfoScore => _localizedValues[locale.languageCode]['userInfoScore'];
  String get channelsTextHead => _localizedValues[locale.languageCode]['channelsTextHead'];
  String get randChatHead => _localizedValues[locale.languageCode]['randChatHead'];
  String get randChatGender => _localizedValues[locale.languageCode]['randChatGender'];
  String get randChatAgeMin => _localizedValues[locale.languageCode]['randChatAgeMin'];
  String get randChatAgeMax => _localizedValues[locale.languageCode]['randChatAgeMax'];
  String get randChatClose => _localizedValues[locale.languageCode]['randChatClose'];
  String get randChatAchiveLike => _localizedValues[locale.languageCode]['randChatAchiveLike'];
  String get randChatAchiveDislike => _localizedValues[locale.languageCode]['randChatAchiveDislike'];
  String get randCharFriendRequest => _localizedValues[locale.languageCode]['randCharFriendRequest'];
  String get randChatAllowFiendship => _localizedValues[locale.languageCode]['randChatAllowFiendship'];
  String get randChatDenyFriendship => _localizedValues[locale.languageCode]['randChatDenyFriendship'];
  String get randChatLeave => _localizedValues[locale.languageCode]['randChatLeave'];
//String get  => _localizedValues[locale.languageCode][''];
//String get  => _localizedValues[locale.languageCode][''];

}
class GLocalizationsDelegate extends LocalizationsDelegate<GLocalizations> {
  const GLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<GLocalizations> load(Locale locale) {
    return SynchronousFuture<GLocalizations>(GLocalizations(locale));
  }

  @override
  bool shouldReload(GLocalizationsDelegate old) => false;
}





