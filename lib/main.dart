
import 'dart:convert';

import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/ChannelsListBody.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/screens/StartScreen.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'locale/GLocalization.dart';


void main() async {
  WidgetsFlutterBinding
      .ensureInitialized();
  SystemChrome
      .setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitUp]);
  SystemChrome
      .setEnabledSystemUIOverlays([]);

  runApp(
      MaterialApp(
        supportedLocales: const <Locale>[
          Locale('en', ''),
          Locale('ru', '')
        ],
        localizationsDelegates: const [
          const GLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        themeMode: ThemeMode.system,
        home: StartScreen(),
      )
  );
}

