


import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/base/MsgBody.dart';
import 'package:anonymousmessenger/data/PrivateChatInfo.dart';
import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/model/body/FriendRequestBody.dart';
import 'package:anonymousmessenger/model/body/SetMarkRequestBody.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/MessageContentBody.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:anonymousmessenger/widgets/Dialog.dart';
import 'package:anonymousmessenger/widgets/SimpleButton.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Chat extends StatefulWidget {
  final bool anonymous;
  final Function closeAction;
  final Function resetScreen;

  Chat({this.anonymous = false, this.closeAction, this.resetScreen});

  @override
  State<StatefulWidget> createState() => _ChatState();


}

class _ChatState extends State<Chat> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _textEditingControllerMsg = TextEditingController();
  String _msg = "";
  double h, w, textSz;


  @override
  Widget build(BuildContext context) {


    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    textSz = h * 0.025;
    return Column(
      children: <Widget>[
        headBuild(context),
        Flexible(
          child: ListView(
            controller: _scrollController,
            children: <Widget>[
              for(MessageContentBody b in PrivateChatInfo.getInfo().msgs)
                msgBuild(context: context, b: b),
              awaitableMessagesChain(context),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              height: textSz * 2,
              width: w - textSz * 3,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: CColors.Main1C,
              ),
              child: EditableText(
                controller: _textEditingControllerMsg,

                onSubmitted: (value) {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  setState(() {_msg = value;});
                },
                onEditingComplete: () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  setState(() {});
                },
                focusNode: FocusNode(),
                style: TextStyle(
                  fontSize: textSz,
                  color: CColors.LightA,
                ),
                cursorColor: CColors.LightA,
                backgroundCursorColor: CColors.LightA,
                selectionColor: CColors.Main2E,
              ),
            ),
            Spacer(),
            SimpleButton(
              child: FlatButton(
                child: Icon(Icons.send, color: CColors.LightA, size: textSz,),
                onPressed: _procSend(context),
                color: CColors.Main1C,
              ),
              minWidth: textSz * 2,
              height: textSz * 2,
            ),
          ],
        ),
      ],
    );
  }

  Widget headBuild(BuildContext context) {
    return Container(
      height: h * 0.1,
      width: w,
      color: CColors.Main1C,
      child: Row(
        children: <Widget>[
          SimpleButton(
            child: FlatButton(
              child: Icon(Icons.person_add, color: PrivateChatInfo.getInfo().friendshipRequestSended ? CColors.OrangeA : CColors.LightA, size: textSz,),
              onPressed: _procAddFriend(context),
              color: CColors.Main1C,
            ),
            minWidth: textSz * 2,
            height: textSz * 2,
          ),
          SimpleButton(
            child: FlatButton(
              child: Icon(Icons.thumb_up, color: PrivateChatInfo.getInfo().likeSetted ? CColors.OrangeA : CColors.LightA, size: textSz,),
              onPressed: _procLike(context),
              color: CColors.Main1C,
            ),
            minWidth: textSz * 2,
            height: textSz * 2,
          ),
          SimpleButton(
            child: FlatButton(
              child: Icon(Icons.thumb_down, color: PrivateChatInfo.getInfo().dislikeSetted ? CColors.OrangeA : CColors.LightA, size: textSz,),
              onPressed: _procDisLike(context),
              color: CColors.Main1C,
            ),
            minWidth: textSz * 2,
            height: textSz * 2,
          ),
          Spacer(),
          SimpleButton(
            child: FlatButton(
              child: Icon(Icons.close, color: CColors.LightA, size: textSz,),
              onPressed: () {
                procAction(
                  text: GLocalizations.of(context).randChatClose,
                  context: context,
                  onNo: () {},
                  showOk: false,
                  showNo: true,
                  showYes: true,
                  onYes: widget.closeAction,
                );

              },
              color: CColors.Main1C,
            ),
            minWidth: textSz * 2,
            height: textSz * 2,
          ),
        ],
      ),
    );
  }

  Widget awaitableMessagesChain(BuildContext context) {
    return FutureBuilder<Message>(
      future: Client.getInst().getLastMsg(), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot<Message> snapshot) {
        Widget data = Container(
          width: w * 0.6,
        );
        if (snapshot.hasData) {
          Message m = snapshot.data;

          if(m.Command == Command.SEND_ANONYMOUS_MSG) {
            MessageContentBody b = m.Body as MessageContentBody;
            PrivateChatInfo.getInfo().msgs.add(m.Body as MessageContentBody);
            if (PrivateChatInfo.getInfo().msgs.length > 0) {
              _scrollController.jumpTo(_scrollController.position.maxScrollExtent + textSz);
            }
            data = Container(
              width: w * 0.6,
              child: Column(
                crossAxisAlignment: (b.From == UserInfo.getInfo().name ?  CrossAxisAlignment.end : CrossAxisAlignment.start),
                children: <Widget>[
                  msgBuild(context: context, b: m.Body as MessageContentBody),
                  awaitableMessagesChain(context),
                ],
              ),
            );
          }
          if(m.Command == Command.SET_USER_MARK) {
            SetMarkRequestBody b = m.Body as SetMarkRequestBody;
            String text = b.Count > 0
                ? GLocalizations.of(context).randChatAchiveLike
                : GLocalizations.of(context).randChatAchiveDislike;
          }
          if(m.Command == Command.REQUEST_FOR_FRIENDSHIP) {
            data = buildForm(
              context: context,
              text: GLocalizations.of(context).randCharFriendRequest,
              showYes: true,
              showNo: true,
              onYes: _procYesFriend(context),
              onNo: _procNoFriend(context),
            );
          }
          if(m.Command == Command.ALLLOW_FRIENDSHIP) {
            data = buildForm(
              context: context,
              text: GLocalizations.of(context).randChatAllowFiendship,
              showOk: true,
              onOk: _procOkFriend(context),
            );
          }
          if(m.Command == Command.DENY_FRIENDSHIP) {
            data = buildForm(
                context: context,
                text: GLocalizations.of(context).randChatDenyFriendship,
                showOk: true,
                onOk: _procOkFriend(context),
            );

          }
          if(m.Command == Command.LEAVE_CHAT) {
            data = buildForm(
              context: context,
              text: GLocalizations.of(context).randChatLeave,
              showOk: true,
              onOk: _procLeave(context),
            );
          }
        }

//        if (PrivateChatInfo.getInfo().msgs.length > 0) {
//          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
//        }
        return data;
      },
    );
  }

  Widget msgBuild({BuildContext context, MessageContentBody b}) {
    DateTime converted = DateTime.parse(b.Time.replaceAll("XYZ", ""));
    converted = converted.add(Duration(hours: DateTime.now().timeZoneOffset.inHours));
    return Padding(
      padding: EdgeInsets.symmetric(vertical: textSz / 2, horizontal: 3),
      child: Column(

        crossAxisAlignment: (b.From == UserInfo.getInfo().name ?  CrossAxisAlignment.end : CrossAxisAlignment.start),
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: (b.From == UserInfo.getInfo().name ?  CColors.OrangeA : CColors.Main1C),
              ),
              width: w * 0.6,
              alignment: Alignment.centerLeft,
              child: Column(
                children: <Widget>[
                  if (!widget.anonymous)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          b.From,
                          softWrap: true,
                          style: TextStyle(
                            color: CColors.Main2E,
                            fontSize: textSz,
                          ),
                        ),
                      ],
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        b.Text,
                        softWrap: true,
                        style: TextStyle(
                          color: CColors.LightA,
                          fontSize: textSz,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        converted.year.toString() + "-"
                            + converted.month.toString() + "-"
                            + converted.day.toString() + " "
                            + converted.hour.toString() + ":"
                            + (converted.minute.toString().length == 1
                            ? "0" + converted.minute.toString()
                            : converted.minute.toString()),
                        softWrap: true,
                        style: TextStyle(
                          color: CColors.DarkA,
                          fontSize: textSz,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ),
        ],
      ),
    );

  }

  Widget buildForm({
    BuildContext context,
    String text,
    bool showYes = false,
    bool showNo = false,
    bool showOk = false,
    bool showCancel = false,
    VoidCallback onYes,
    VoidCallback onNo,
    VoidCallback onCancel,
    VoidCallback onOk}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: textSz / 2),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: CColors.Main1C,
              ),
              width: w * 0.6,
              height: h * 0.3,
              alignment: Alignment.centerLeft,
              child: Column(
                children: <Widget>[
                  Text(
                    text,
                    softWrap: true,
                    style: TextStyle(
                      color: CColors.LightA,
                      fontSize: textSz,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Spacer(),

                      if(showOk)
                        SimpleButton(
                          child: FlatButton(
                            child: SimpleText(
                              text: GLocalizations.of(context).dialogActionOk,
                              color: CColors.LightA,
                              size: textSz,
                            ),
                            onPressed: onOk,
                            color: CColors.Main1C,
                          ),
                          minWidth: textSz * 2,
                          height: textSz * 2,
                        ),

                      if(showCancel)
                        SimpleButton(
                          child: FlatButton(
                            child: SimpleText(
                              text: GLocalizations.of(context).dialogActionCancel,
                              color: CColors.LightA,
                              size: textSz,
                            ),
                            onPressed: onCancel,
                            color: CColors.Main1C,
                          ),
                          minWidth: textSz * 2,
                          height: textSz * 2,
                        ),

                      if(showYes)
                        SimpleButton(
                          child: FlatButton(
                            child: SimpleText(
                              text: GLocalizations.of(context).dialogActionYes,
                              color: CColors.LightA,
                              size: textSz,
                            ),
                            onPressed: onYes,
                            color: CColors.Main1C,
                          ),
                          minWidth: textSz * 2,
                          height: textSz * 2,
                        ),

                      if(showNo)
                        SimpleButton(
                          child: FlatButton(
                            child: SimpleText(
                              text: GLocalizations.of(context).dialogActionNo,
                              color: CColors.LightA,
                              size: textSz,
                            ),
                            onPressed: onNo,
                            color: CColors.Main1C,
                          ),
                          minWidth: textSz * 2,
                          height: textSz * 2,
                        ),
                      Spacer(),
                    ],
                  ),
                ],
              ),
            ),
          ]
      ),
    );
  }


  VoidCallback _procSend(BuildContext context) {
    return () {
      if (widget.anonymous) {
        MessageContentBody body = MessageContentBody(
          ChannelId: PrivateChatInfo.getInfo().channelId,
          From: UserInfo.getInfo().name,
          Text: _textEditingControllerMsg.text,
          Time: "XYZ" + DateTime.now().toUtc().toIso8601String(),
          PrivateChannelId: PrivateChatInfo.getInfo().channelId,
        );
        Client.getInst().sendMsg(
            Message(
              Command: Command.SEND_ANONYMOUS_MSG,
              Body: body
            ),
        );
        PrivateChatInfo.getInfo().msgs.add(body);
      }
      setState(() {

      });
    };
  }


  VoidCallback _procLike(BuildContext context) {
    return() {
      if (!PrivateChatInfo.getInfo().likeSetted && !PrivateChatInfo.getInfo().dislikeSetted) {
        Client.getInst().sendMsg(
          Message(
              Command: Command.SET_USER_MARK,
              Body: SetMarkRequestBody(
                PChannelId: PrivateChatInfo.getInfo().channelId,
                Count: 20,
                UserTo: "",
              )
          ),
        );
        PrivateChatInfo.getInfo().likeSetted = true;
      } else if (PrivateChatInfo.getInfo().likeSetted) {
        Client.getInst().sendMsg(
          Message(
              Command: Command.SET_USER_MARK,
              Body: SetMarkRequestBody(
                PChannelId: PrivateChatInfo.getInfo().channelId,
                Count: -20,
                UserTo: "",
              )
          ),
        );
        PrivateChatInfo.getInfo().likeSetted = false;
      }
      setState(() {

      });
    };
  }

  VoidCallback _procDisLike(BuildContext context) {
    return() {
      if (!PrivateChatInfo.getInfo().likeSetted && !PrivateChatInfo.getInfo().dislikeSetted) {
        Client.getInst().sendMsg(
          Message(
              Command: Command.SET_USER_MARK,
              Body: SetMarkRequestBody(
                PChannelId: PrivateChatInfo.getInfo().channelId,
                Count: -20,
                UserTo: "",
              )
          ),
        );
        PrivateChatInfo.getInfo().dislikeSetted = true;
      } else if (PrivateChatInfo.getInfo().dislikeSetted) {
        Client.getInst().sendMsg(
          Message(
              Command: Command.SET_USER_MARK,
              Body: SetMarkRequestBody(
                PChannelId: PrivateChatInfo.getInfo().channelId,
                Count: 20,
                UserTo: "",
              )
          ),
        );
        PrivateChatInfo.getInfo().dislikeSetted = false;
      }
      setState(() {

      });
    };
  }

  VoidCallback _procAddFriend(BuildContext context) {
    return() {
      if (PrivateChatInfo.getInfo().friendshipRequestSended) return;
      Client.getInst().sendMsg(
          Message(
            Command: Command.REQUEST_FOR_FRIENDSHIP,
            Body: FriendRequestBody(
              Allow: true,
              FPChannelId: PrivateChatInfo.getInfo().channelId,
              UserTo: "",
              UserFrom: "",
            ),
          )
      );
      setState(() {
        PrivateChatInfo.getInfo().friendshipRequestSended = true;
      });
    };
  }


  VoidCallback _procYesFriend(BuildContext context) {
    return () {

      Client.getInst().sendMsg(
        Message(
          Command: Command.ALLLOW_FRIENDSHIP,
          Body: FriendRequestBody(
            Allow: true,
            FPChannelId: PrivateChatInfo.getInfo().channelId,
            UserTo: "",
            UserFrom: "",
          ),
        )
      );
      Client.getInst().sendMsg(
          Message(
            Command: Command.NO_MSG,
            Body: null,
          )
      );
        PrivateChatInfo.getInfo().friendshipRequestSended = false;
      setState(() {
      });
    };
  }


  VoidCallback _procNoFriend(BuildContext context) {
    return () {
      Client.getInst().sendMsg(
          Message(
            Command: Command.DENY_FRIENDSHIP,
            Body: FriendRequestBody(
              Allow: false,
              FPChannelId: PrivateChatInfo.getInfo().channelId,
              UserTo: "",
              UserFrom: "",
            ),
          )
      );
      Client.getInst().sendMsg(
          Message(
            Command: Command.NO_MSG,
            Body: null,
          )
      );
        PrivateChatInfo.getInfo().friendshipRequestSended = false;
      setState(() {
      });
    };
  }

  VoidCallback _procOkFriend(BuildContext context) {
    return () {
      Client.getInst().sendMsg(
          Message(
            Command: Command.NO_MSG,
            Body: null,
          )
      );
      setState(() {

      });
    };
  }


  VoidCallback _procLeave(BuildContext context) {
    return () {
      widget.resetScreen();
    };
  }
}