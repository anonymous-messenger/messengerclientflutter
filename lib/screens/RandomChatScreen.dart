





import 'dart:async';

import 'package:anonymousmessenger/data/PrivateChatInfo.dart';
import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/model/body/MessageContentBody.dart';
import 'package:anonymousmessenger/model/body/RandChatRequestBody.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:anonymousmessenger/widgets/Dialog.dart';
import 'package:anonymousmessenger/widgets/SimpleButton.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'ChatWidget.dart';
import 'MenuNavigator.dart';




class RandomChatScreen extends StatefulWidget {


  @override
  State<StatefulWidget> createState() => _RandomChatScreen();

}



class _RandomChatScreen extends State<RandomChatScreen> {
  var _genders = const <String>[
    "male",
    "none",
    "female"
  ];
  double h, w, textSz;

  int _ageMax = 150, _ageMin = 18, _curAgeMax = 20, _curAgeMin = 20;

  int _selectedGenderId = -1;
  String _gender;


  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    textSz = h * 0.025;

    return Scaffold(
      bottomNavigationBar: MenuNavigator(),
      backgroundColor: CColors.DarkA,
      body: PrivateChatInfo.getInfo().connected ? Chat(anonymous: true, closeAction: () {
        Client.getInst().sendMsg(
          Message(
            Command: Command.LEAVE_CHAT,
            Body:  MessageContentBody(
              ChannelId: PrivateChatInfo.getInfo().channelId,
              From: "",
              Text: "",
              Time: "",
              PrivateChannelId: PrivateChatInfo.getInfo().channelId,
            ),
          ),
        );

        PrivateChatInfo.getInfo().msgs.clear();
        PrivateChatInfo.getInfo().connected = false;
        PrivateChatInfo.getInfo().awaitConnection = false;
        setState(() {

        });
      }, resetScreen: () {
        PrivateChatInfo.getInfo().msgs.clear();
        PrivateChatInfo.getInfo().connected = false;
        PrivateChatInfo.getInfo().awaitConnection = false;
        PrivateChatInfo.getInfo().friendshipRequestSended = false;
        setState(() {

        });
      },) : (PrivateChatInfo.getInfo().awaitConnection
          ? FutureBuilder<Message>(
        future: Client.getInst().getLastMsg(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.Command == Command.RAND_CHAT_REQUEST) {
              RandChatRequestBody b = snapshot.data.Body as RandChatRequestBody;
              PrivateChatInfo.getInfo().channelId = b.PChannelId;

              PrivateChatInfo.getInfo().connected = true;
              PrivateChatInfo.getInfo().likeSetted = false;
              PrivateChatInfo.getInfo().dislikeSetted = false;
              PrivateChatInfo.getInfo().friendshipRequestSended = false;

            }
            return Chat(anonymous: true, closeAction: () {
              Client.getInst().sendMsg(
                Message(
                  Command: Command.LEAVE_CHAT,
                  Body:  MessageContentBody(
                    ChannelId: PrivateChatInfo.getInfo().channelId,
                    From: "",
                    Text: "",
                    Time: "",
                    PrivateChannelId: PrivateChatInfo.getInfo().channelId,
                  ),
                ),
              );

              PrivateChatInfo.getInfo().msgs.clear();
              PrivateChatInfo.getInfo().connected = false;
              PrivateChatInfo.getInfo().awaitConnection = false;
              PrivateChatInfo.getInfo().friendshipRequestSended = false;
              setState(() {

              });
            }, resetScreen: () {
              PrivateChatInfo.getInfo().msgs.clear();
              PrivateChatInfo.getInfo().connected = false;
              PrivateChatInfo.getInfo().awaitConnection = false;
              PrivateChatInfo.getInfo().friendshipRequestSended = false;
              setState(() {

              });
            },);
          } else {
            return buildLoading(context);
          }
        },
      )
          : buildForm(context))
    );
  }

  Widget buildLoading(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Spacer(),
          Container(
            width: w*0.5,
            height: w*0.5,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(CColors.OrangeA),
              backgroundColor: CColors.Main1C,
              strokeWidth: textSz * 2,
            )
          ),
          Container(
            width: w*0.5,
            height: w*0.5,
          ),
          SimpleButton(
            child: FlatButton(
              child: Icon(Icons.cancel, color: CColors.LightA, size: textSz,),
              onPressed: _procCancel(context),
              color: CColors.Main1C,
            ),
            minWidth: 0.07 * h,
            height: 0.07 * h,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget buildForm(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacer(flex: 8,),
          SimpleText(
            text: GLocalizations.of(context).randChatHead,
            color: CColors.LightA,
            size: 2 * textSz,
          ),
          Spacer(),
          Container(
            width: 0.8 * w,
            height: 0.4 * h,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                border: Border.all(
                  color: CColors.OrangeA,
                  width: 1,
                )
            ),
            child: Column(
              children: <Widget>[
                Spacer(flex: 4,),
                SimpleText(
                  text: GLocalizations.of(context).randChatAgeMin,
                  color: CColors.LightA,
                  size: textSz,
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Spacer(),
                    SimpleButton(
                      child: FlatButton(
                        child: Icon(Icons.arrow_left, color: CColors.LightA, size: textSz * 2,),
                        onPressed: () {setState(() {
                          if (_curAgeMin > _ageMin) _curAgeMin--;
                        });},
                        color: CColors.Main1C,
                      ),
                      minWidth: 0.8 * w * 0.33,
                      height: textSz * 2,
                    ),
                    Spacer(),
                    Container(
                      alignment: Alignment.center,
                      width: 0.8 * w * 0.33,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: CColors.Main1C,
                      ),
                      child: SimpleText(
                        text: _curAgeMin.toString(),
                        color: CColors.LightA,
                        size: textSz,
                      ),
                    ),
                    Spacer(),
                    SimpleButton(
                      child: FlatButton(
                        child: Icon(Icons.arrow_right, color: CColors.LightA, size: textSz * 2,),
                        onPressed: () {setState(() {
                          if (_curAgeMin < _ageMax && _curAgeMin < _curAgeMax) _curAgeMin++;
                        });},
                        color: CColors.Main1C,
                      ),
                      minWidth: 0.8 * w * 0.33,
                      height: textSz * 2,
                    ),
                    Spacer(),
                  ],
                ),
                Spacer(flex: 4,),
                SimpleText(
                  text: GLocalizations.of(context).randChatAgeMax,
                  color: CColors.LightA,
                  size: textSz,
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Spacer(),
                    SimpleButton(
                      child: FlatButton(
                        child: Icon(Icons.arrow_left, color: CColors.LightA, size: textSz * 2,),
                        onPressed: () {setState(() {
                          if (_curAgeMax > _ageMin && _curAgeMin < _curAgeMax) _curAgeMax--;
                        });},
                        color: CColors.Main1C,
                      ),
                      minWidth: 0.8 * w * 0.33,
                      height: textSz * 2,
                    ),
                    Spacer(),
                    Container(
                      alignment: Alignment.center,
                      width: 0.8 * w * 0.33,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: CColors.Main1C,
                      ),
                      child: SimpleText(
                        text: _curAgeMax.toString(),
                        color: CColors.LightA,
                        size: textSz,
                      ),
                    ),
                    Spacer(),
                    SimpleButton(
                      child: FlatButton(
                        child: Icon(Icons.arrow_right, color: CColors.LightA, size: textSz * 2,),
                        onPressed: () {setState(() {
                          if (_curAgeMax < _ageMax) _curAgeMax++;
                        });},
                        color: CColors.Main1C,
                      ),
                      minWidth: 0.8 * w * 0.33,
                      height: textSz * 2,
                    ),
                    Spacer(),
                  ],
                ),
                Spacer(flex: 4,),
                SimpleText(
                  text: GLocalizations.of(context).randChatGender,
                  color: CColors.LightA,
                  size: textSz,
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Spacer(),
                    for (int i = 0; i < _genders.length; ++i)
                      SimpleButton(
                        child: FlatButton(
                          child: Row(
                            children: <Widget>[
                              if (i == 0)
                                Icon(MdiIcons.humanMale, color: CColors.LightA),
                              if (i == 1)
                                Icon(Icons.close, color: CColors.LightA),
                              if (i == 2)
                                Icon(MdiIcons.humanFemale, color: CColors.LightA),
                            ],
                          ),
                          onPressed: () {setState(() {
                            _selectedGenderId = i;
                          });},
                          color: _selectedGenderId == i ? CColors.OrangeA : CColors.Main1C,
                        ),
                        minWidth: 0.8 * w * 0.33,
                        height: textSz * 2,
                      ),
                  ],
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            width: 0.8 * w,
            child: Row(
              children: <Widget>[
                SimpleButton(
                  child: FlatButton(
                    child: Icon(Icons.search, color: CColors.LightA, size: textSz,),
                    onPressed: _procFind(context),
                    color: CColors.Main1C,
                  ),
                  minWidth: 0.8 * w,
                  height: 0.07 * h,
                ),
              ],
            ),
          ),
          Spacer(flex: 8,),
        ],
      ),
    );
  }

  VoidCallback _procFind(BuildContext context) {
    return () async {
      if (_selectedGenderId == -1) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorSelectGender,
          onOk: () {},
        );
        return;
      }
      PrivateChatInfo.getInfo().awaitConnection = true;
      Client.getInst().sendMsg(
        Message(
          Command: Command.RAND_CHAT_REQUEST,
          Body: RandChatRequestBody(
            Gender: _genders[_selectedGenderId],
            MaxAge: _curAgeMax,
            MinAge: _curAgeMin,
            Abort: false,
            PChannelId: -1,
          ),
        )
      );

      setState(() {

      });
    };
  }

  VoidCallback _procCancel(BuildContext context) {
    return () {
      PrivateChatInfo.getInfo().awaitConnection = false;

      Client.getInst().sendMsg(
          Message(
            Command: Command.RAND_CHAT_REQUEST,
            Body: RandChatRequestBody(
              Gender: _genders[_selectedGenderId],
              MaxAge: _curAgeMax,
              MinAge: _curAgeMin,
              Abort: true,
              PChannelId: -1,
            ),
          )
      );
      setState(() {

      });
    };
  }

}