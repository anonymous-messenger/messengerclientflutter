





import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/model/body/AuthRegAnswerBody.dart';
import 'package:anonymousmessenger/model/body/AuthRegBody.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:anonymousmessenger/widgets/Dialog.dart';
import 'package:anonymousmessenger/widgets/SimpleButton.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'EmptyScreen.dart';
import 'MenuScreen.dart';



class RegisterScreen extends StatefulWidget {


  @override
  State<StatefulWidget> createState() => _RegisterScreen();

}



class _RegisterScreen extends State<RegisterScreen> {
  var _genders = const <String>[
    "male",
    "none",
    "female"
  ];

  int _ageMax = 150, _ageMin = 18, _age = 20;

  int _selectedGenderId = -1;
  String _name, _pass, _gender;
  TextEditingController _textEditingControllerName = TextEditingController();
  TextEditingController _textEditingControllerPass = TextEditingController();


  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double textSz = h * 0.025;
    return Scaffold(
      backgroundColor: CColors.DarkA,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(flex: 8,),
            SimpleText(
              text: GLocalizations.of(context).registerScreenHead,
              color: CColors.LightA,
              size: 2 * textSz,
            ),
            Spacer(),
            Container(
              width: 0.8 * w,
              height: 0.4 * h,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  border: Border.all(
                    color: CColors.OrangeA,
                    width: 1,
                  )
              ),
              child: Column(
                children: <Widget>[
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).registerScreenUserName,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: CColors.Main1C,
                    ),
                    child: EditableText(
                      controller: _textEditingControllerName,
                      onSubmitted: (value) {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {_name = value;});
                      },
                      onEditingComplete: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {});
                      },
                      focusNode: FocusNode(),
                      style: TextStyle(
                        fontSize: textSz,
                        color: CColors.LightA,
                      ),
                      cursorColor: CColors.LightA,
                      backgroundCursorColor: CColors.LightA,
                      selectionColor: CColors.Main2E,
                    ),
                  ),
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).registerScreenAge,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      Spacer(),
                      SimpleButton(
                        child: FlatButton(
                          child: Icon(Icons.arrow_left, color: CColors.LightA, size: textSz * 2,),
                          onPressed: () {setState(() {
                            if (_age > _ageMin) _age--;
                          });},
                          color: CColors.Main1C,
                        ),
                        minWidth: 0.8 * w * 0.33,
                        height: textSz * 2,
                      ),
                      Spacer(),
                      Container(
                        alignment: Alignment.center,
                        width: 0.8 * w * 0.33,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          color: CColors.Main1C,
                        ),
                        child: SimpleText(
                          text: _age.toString(),
                          color: CColors.LightA,
                          size: textSz,
                        ),
                      ),
                      Spacer(),
                      SimpleButton(
                        child: FlatButton(
                          child: Icon(Icons.arrow_right, color: CColors.LightA, size: textSz * 2,),
                          onPressed: () {setState(() {
                            if (_age < _ageMax) _age++;
                          });},
                          color: CColors.Main1C,
                        ),
                        minWidth: 0.8 * w * 0.33,
                        height: textSz * 2,
                      ),
                      Spacer(),
                    ],
                  ),
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).registerScreenGender,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      Spacer(),
                      for (int i = 0; i < _genders.length; ++i)
                        SimpleButton(
                          child: FlatButton(
                            child: Row(
                              children: <Widget>[
                                if (i == 0)
                                  Icon(MdiIcons.humanMale, color: CColors.LightA),
                                if (i == 1)
                                  Icon(Icons.close, color: CColors.LightA),
                                if (i == 2)
                                  Icon(MdiIcons.humanFemale, color: CColors.LightA),
                              ],
                            ),
                            onPressed: () {setState(() {
                              _selectedGenderId = i;
                            });},
                            color: _selectedGenderId == i ? CColors.OrangeA : CColors.Main1C,
                          ),
                          minWidth: 0.8 * w * 0.33,
                          height: textSz * 2,
                        ),
                    ],
                  ),
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).registerScreenPassword,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: CColors.Main1C,
                    ),
                    child: EditableText(
                      controller: _textEditingControllerPass,

                      onSubmitted: (value) {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {_pass = value;});
                      },
                      onEditingComplete: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {});
                      },
                      focusNode: FocusNode(),
                      style: TextStyle(
                        fontSize: textSz,
                        color: CColors.LightA,
                      ),
                      cursorColor: CColors.LightA,
                      backgroundCursorColor: CColors.LightA,
                      selectionColor: CColors.Main2E,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              width: 0.8 * w,
              child: Row(
                children: <Widget>[
                  SimpleButton(
                    child: FlatButton(
                      child: Icon(Icons.arrow_back, color: CColors.LightA, size: textSz,),
                      onPressed: _procBack(context),
                      color: CColors.Main1C,
                    ),
                    minWidth: 0.35 * w,
                    height: 0.07 * h,
                  ),
                  Spacer(),
                  SimpleButton(
                    child: FlatButton(
                      child: Icon(Icons.input, color: CColors.LightA, size: textSz,),
                      onPressed: _procRegister(context),
                      color: CColors.Main1C,
                    ),
                    minWidth: 0.35 * w,
                    height: 0.07 * h,
                  ),
                ],
              ),
            ),
            Spacer(flex: 8,),
          ],
        ),
      ),
    );
  }

  VoidCallback _procRegister(BuildContext context) {
    return () async {
      _pass = _textEditingControllerPass.text;
      _name = _textEditingControllerName.text;

      if (_pass.isEmpty) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorFillPass,
          onOk: () {},
        );
        return;
      }

      if (_name.isEmpty) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorFillName,
          onOk: () {},
        );
        return;
      }

      if (_selectedGenderId == -1) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorSelectGender,
          onOk: () {},
        );
        return;
      }

      await Client.getInst().connect();
      Client.getInst().startListen();

      Message m = Message(
          Command: Command.REGISTER,
          Body: AuthRegBody(
            UserName: _name,
            Pass: _pass,
            Age: _age,
            Gender: _genders[_selectedGenderId],
          )
      );

      Client.getInst().sendMsg(m);
      Message msg = await Client.getInst().getLastMsg();

      if (msg.Body is AuthRegAnswerBody) {
        AuthRegAnswerBody b = msg.Body;
        if (b.Success) {
          await UserInfo.getInfo().update();

          Navigator.push(
              context, MaterialPageRoute(
            builder: (context) => EmptyScreen(),
          )
          );

          Navigator.push(
              context, MaterialPageRoute(
              builder: (context) => MenuScreen(),
          )
          );


        } else {
          procAction(
            context: context,
            text: b.Desc,
            onOk: () {},
          );
        }
      }


    };
  }


  VoidCallback _procBack(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }



}