


import 'package:anonymousmessenger/data/MessageChannels.dart';
import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:anonymousmessenger/widgets/Dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'ChannelsScreen.dart';
import 'MenuScreen.dart';
import 'RandomChatScreen.dart';

class MenuNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double iconSz = w * 0.1;
    return Container(
      height: 0.1 * h,
      width: w,
      decoration: BoxDecoration(color: CColors.Main1C),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacer(),
          GestureDetector(
            onTap: _setInfoView(context),
            child: Icon(Icons.account_box, color: CColors.OrangeA, size: iconSz,),
          ),
          Spacer(),
          GestureDetector(
            onTap: _setMessageChannels(context),
            child: Icon(Icons.message, color: CColors.OrangeA, size: iconSz,),
          ),
          Spacer(),
          GestureDetector(
            onTap: _setRandChatSearch(context),
            child: Icon(Icons.search, color: CColors.OrangeA, size: iconSz,),
          ),
          Spacer(),
          GestureDetector(
            onTap: procActionCallback(
                context: context,
                text: GLocalizations.of(context).dialogExitAppText,
                showOk: false,
                showYes: true,
                showNo: true,
                onYes: () {
                  Client.getInst().sendMsg(Message(Command: Command.DISCONNECT));
                  SystemNavigator.pop();
                },
                onNo: () {}
                ),
            child: Icon(Icons.exit_to_app, color: CColors.OrangeA, size: iconSz,),
          ),
          Spacer(),
        ],

      ),
    );
  }

  VoidCallback _setInfoView(BuildContext context) {
    return () async {
      Navigator.pop(context);

      UserInfo.getInfo();
      await UserInfo.getInfo().update();

      Navigator.push(
          context, MaterialPageRoute(
          builder: (context) => MenuScreen()
      )
      );
    };
  }

  VoidCallback _setMessageChannels(BuildContext context) {
    return () async {
      Navigator.pop(context);

      MessageChannels.getInfo();
      await MessageChannels.getInfo().update();

      Navigator.push(
          context, MaterialPageRoute(
          builder: (context) => ChannelsScreen()
      )
      );
    };
  }

  VoidCallback _setRandChatSearch(BuildContext context) {
    return () async {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(
          builder: (context) => RandomChatScreen()
      )
      );
    };
  }

}