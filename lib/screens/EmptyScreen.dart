

import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CColors.OrangeA,
    );
  }

}