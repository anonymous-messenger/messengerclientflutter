



import 'package:anonymousmessenger/data/MessageChannels.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'file:///D:/Development/Projects/Android/anonymous_messenger/lib/model/body/ChannelsListBody.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'MenuNavigator.dart';

class ChannelsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChannelsScreen();
}


class _ChannelsScreen extends State<ChannelsScreen> {
  double h, w, textSz;

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    textSz = h * 0.025;
    return Scaffold(
      bottomNavigationBar: MenuNavigator(),
      backgroundColor: CColors.DarkA,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SimpleText(
              color: CColors.LightA,
              size: textSz * 2,
              text: GLocalizations.of(context).channelsTextHead,
            ),
            Container(
              height: h * 0.8,
              width: w,
              child: ListView(
                children: <Widget>[
                  for (Channel c in MessageChannels.getInfo().body.Channels)
                    buildChannelRow(context, c.Name),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildChannelRow(BuildContext context, String text) {
    return Container(
      width: w,
      height: 0.1 * h,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(1),
        color: CColors.OrangeA,
      ),
      child: GestureDetector(
        child: Row(
          children: <Widget>[
            Container(
              height: h * 0.1,
              width: h * 0.1,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Container(
                      decoration: ShapeDecoration(
                        color: CColors.Main1C,
                        shape: CircleBorder(
                          side: BorderSide(
                            color: CColors.Main1C,
                          ),
                        ),
                      ),
                    ),
                  ),

                  Center(
                    child: SimpleText(
                      color: CColors.LightA,
                      size: textSz * 2,
                      text: text.substring(0, 1),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SimpleText(
                  color: CColors.LightA,
                  size: textSz,
                  text: text,
                ),
                Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
  }

}