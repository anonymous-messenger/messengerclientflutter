


import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/screens/MenuNavigator.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MenuScreen();
}


class _MenuScreen extends State<MenuScreen> {
  double h, w, textSz;
  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    textSz = h * 0.025;
    return Scaffold(
      bottomNavigationBar: MenuNavigator(),
      backgroundColor: CColors.DarkA,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SimpleText(
              text: GLocalizations.of(context).userInfoHead,
              color: CColors.LightA,
              size: textSz * 2,
            ),
            Spacer(),
            buildRow(
              context: context,
              text: GLocalizations.of(context).userInfoName,
              info: UserInfo.getInfo().name,
            ),
            buildRow(
              context: context,
              text: GLocalizations.of(context).userInfoGender,
              info: UserInfo.getInfo().gender,
            ),
            buildRow(
              context: context,
              text: GLocalizations.of(context).userInfoAge,
              info: UserInfo.getInfo().age.toString(),
            ),
            buildRow(
              context: context,
              text: GLocalizations.of(context).userInfoScore,
              info: UserInfo.getInfo().score.toString(),
            ),
            Spacer(flex: 10,),
          ],
        ),
      ),
    );
  }

  Widget buildRow({BuildContext context, String text, String info}) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Row (
        children: <Widget>[
          Spacer(flex: 4,),
          SimpleText(
            text: text,
            color: CColors.LightA,
            size: textSz,
          ),
          Spacer(),
          Container(
            width: w * 0.6,
            height: h * 0.05,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              color: CColors.Main1C,
            ),
            child: SimpleText(
              text: info,
              color: CColors.LightA,
              size: textSz,
            ),
          ),

        ],
      ),
    );
  }

}