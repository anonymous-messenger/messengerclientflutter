
import 'package:anonymousmessenger/data/UserInfo.dart';
import 'package:anonymousmessenger/locale/GLocalization.dart';
import 'package:anonymousmessenger/model/Command.dart';
import 'package:anonymousmessenger/model/Message.dart';
import 'package:anonymousmessenger/model/body/AuthRegAnswerBody.dart';
import 'package:anonymousmessenger/model/body/AuthRegBody.dart';
import 'package:anonymousmessenger/screens/RegisterScreen.dart';
import 'package:anonymousmessenger/utils/CColors.dart';
import 'package:anonymousmessenger/utils/Client.dart';
import 'package:anonymousmessenger/widgets/Dialog.dart';
import 'package:anonymousmessenger/widgets/SimpleButton.dart';
import 'package:anonymousmessenger/widgets/SimpleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'EmptyScreen.dart';
import 'MenuScreen.dart';

class StartScreen extends StatefulWidget {
  StartScreen({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<StartScreen> {
  TextEditingController _textEditingControllerName = TextEditingController();
  TextEditingController _textEditingControllerPass = TextEditingController();
  String _name = "";
  String _pass = "";

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double textSz = h * 0.025;
    return Scaffold(

      backgroundColor: CColors.DarkA,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(flex: 8,),
            SimpleText(
              text: GLocalizations.of(context).startScreenTextHeadLogin,
              color: CColors.LightA,
              size: 2 * textSz,
            ),
            Spacer(),
            Container(
              width: 0.8 * w,
              height: 0.3 * h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                border: Border.all(
                  color: CColors.OrangeA,
                  width: 1,
                )
              ),
              child: Column(
                children: <Widget>[
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).startScreenTextUserName,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: CColors.Main1C,
                    ),
                    child: EditableText(
                      controller: _textEditingControllerName,
                      onSubmitted: (value) {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {_name = value;});
                      },
                      onEditingComplete: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {});
                      },
                      focusNode: FocusNode(),
                      style: TextStyle(
                        fontSize: textSz,
                        color: CColors.LightA,
                      ),
                      cursorColor: CColors.LightA,
                      backgroundCursorColor: CColors.LightA,
                      selectionColor: CColors.Main2E,
                    ),
                  ),
                  Spacer(flex: 4,),
                  SimpleText(
                    text: GLocalizations.of(context).startScreenTextPassword,
                    color: CColors.LightA,
                    size: textSz,
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      color: CColors.Main1C,
                    ),
                    child: EditableText(
                      controller: _textEditingControllerPass,
                      onSubmitted: (value) {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {_pass = value;});
                      },
                      onEditingComplete: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        setState(() {});
                      },
                      focusNode: FocusNode(),
                      style: TextStyle(
                        fontSize: textSz,
                        color: CColors.LightA,
                      ),
                      cursorColor: CColors.LightA,
                      backgroundCursorColor: CColors.LightA,
                      selectionColor: CColors.Main2E,
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
            Spacer(),
            Container(
              height: h * 0.07,
              width: w * 0.8,
              child: Row(
                children: <Widget>[
                  SimpleButton(
                    height: h * 0.07,
                    minWidth: w * 0.35,
                    child: FlatButton(
                      child: Icon(Icons.account_box, color: CColors.LightA,),
                      onPressed: _procRegister(context),
                      color: CColors.Main1C,
                    ),
                  ),
                  Spacer(),
                  SimpleButton(
                    height: h * 0.07,
                    minWidth: w * 0.35,
                    child: FlatButton(
                      child: Icon(Icons.input, color: CColors.LightA,),
                      onPressed: _procLogin(context),
                      color: CColors.Main1C,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(flex: 8,),
          ],
        ),
      ),
    );
  }


  VoidCallback _procLogin(BuildContext context) {
    return () async {

      _name = _textEditingControllerName.text;
      _pass = _textEditingControllerPass.text;

      if (_pass.isEmpty) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorFillPass,
          onOk: () {},
        );
        return;
      }

      if (_name.isEmpty) {
        procAction(
          context: context,
          text: GLocalizations.of(context).errorFillName,
          onOk: () {},
        );
        return;
      }

      await Client.getInst().connect();
      Client.getInst().startListen();
      Message m = Message(
          Command: Command.AUTH,
          Body: AuthRegBody(
            UserName: _name,
            Pass: _pass,
            Age: 0,
            Gender: "",
          )
      );

      Client.getInst().sendMsg(m);
      Message msg = await Client.getInst().getLastMsg();

      if (msg.Body is AuthRegAnswerBody) {
        AuthRegAnswerBody b = msg.Body;
        if (b.Success) {
          await UserInfo.getInfo().update();

          Navigator.push(
              context, MaterialPageRoute(
            builder: (context) => EmptyScreen(),
          )
          );

          Navigator.push(
              context, MaterialPageRoute(
            builder: (context) => MenuScreen(),
          )
          );
        } else {
          procAction(
            context: context,
            text: b.Desc,
            onOk: () {},
          );
        }
      }
    };
  }




  VoidCallback _procRegister(BuildContext context) {
    return () {
      Navigator.push(
          context, MaterialPageRoute(
          builder: (context) => RegisterScreen()
      )
      );
    };
  }
}


